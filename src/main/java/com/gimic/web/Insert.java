package com.gimic.web;

import com.gimic.web.db.DBConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Insert", urlPatterns = "/Insert")
public class Insert extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        String name = request.getParameter("name");
        String mobile = request.getParameter("mobile");
        String country = request.getParameter("country");

        if(name.equals("")){
            response.getWriter().write("Name is empty");
        }else if(mobile.equals("")){
            response.getWriter().write("mobile is empty");
        }else if(country.equals("")){
            response.getWriter().write("country is empty");
        }else{

            DBConnection.iud("INSERT INTO `user`(`name`,`country`,`mobile`) VALUES('"+name+"','"+country+"','"+mobile+"')");
            response.getWriter().write("Successful");
            response.sendRedirect("index.jsp");

        }

    }
}
