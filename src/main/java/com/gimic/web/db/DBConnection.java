package com.gimic.web.db;

import com.gimic.web.util.ApplicationProperties;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DBConnection {
    private static Connection connection;

    public static Statement getConnection() throws Exception {

            ApplicationProperties properties = ApplicationProperties.getInstance();
            Class.forName(properties.get("sql.connection.driver"));
            connection = DriverManager.getConnection(properties.get("sql.connection.url"), properties.get("sql.connection.username"),
                    properties.get("sql.connection.password"));

        return connection.createStatement();
    }

    public static void iud(String query){
        try{
            getConnection().executeUpdate(query);
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public static ResultSet search(String query) throws Exception{
        return getConnection().executeQuery(query);
    }
}
